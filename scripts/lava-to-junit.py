#!/usr/bin/env python3
# SPDX-FileCopyrightText: <text>Copyright 2022-2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

# Converts lava results files to junit format

import argparse
import yaml

from junit_xml import TestSuite, TestCase


def main():
    parser = argparse.ArgumentParser(
        description='Converts lava test result yaml file into junit format')

    parser.add_argument("log_file", help="lava yaml test result file")
    parser.add_argument("output_file", help="Output Junit log file")
    parser.add_argument(
        "-n", "--name",
        action="store",
        default="Lava test",
        help="Test Suite Name to be used in the output file")

    args = parser.parse_args()

    parse_data(args.log_file, args.output_file, args.name)


def parse_data(log_file_path, export_file, name):
    test_cases = []

    with open(log_file_path, "r") as file:
        data = yaml.safe_load(file)

        for test in data:
            test_cases.append(add_result(name, test))

    ts = TestSuite(name, test_cases)

    with open(export_file, 'w') as f:
        TestSuite.to_file(f, [ts])


def get_test_duration(test_case):
    md = test_case.get('metadata', {})
    duration = float(md.get("duration", 0))

    return duration


def get_test_output(test_case):
    md = test_case.get('metadata', {})

    test_output = md.get('extra', None)

    if test_output is not None:
        test_output = yaml.dump(test_output)

    return test_output


def update_name(test_name, level, md):
    if level != '':
        test_name += "." + level
    else:
        if 'uuid' in md:
            test_name += "." + md['uuid'].split('_', 2)[1]

    return test_name


def get_test_name(test_case):
    md = test_case.get('metadata', {})
    test_name = test_case['name']

    test_extra = md.get('extra', [])
    for extra in test_extra:
        if 'label' in extra and test_name == "http-download":
            test_name = extra['label']

    test_name = update_name(test_name, test_case['level'], md)
    return test_name


def add_result(name, test_case):

    duration = get_test_duration(test_case)
    test_output = get_test_output(test_case)
    test_name = get_test_name(test_case)

    tc = TestCase(test_name,
                  elapsed_sec=duration,
                  classname=name,
                  timestamp=test_case['logged'],
                  stdout=test_output)

    test_result = test_case['result']
    if test_result == 'fail':
        tc.add_failure_info(test_result)
    elif test_result == 'skipped':
        tc.add_skipped_info("skipped")

    return tc


if __name__ == '__main__':
    main()

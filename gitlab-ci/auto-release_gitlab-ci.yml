# SPDX-FileCopyrightText: <text>Copyright 2022-2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT
---
# These jobs create a gitlab release and attached release note based on changes
# to the Changelog. The release note is generated from the git commit history
# and requires knowledge of the current package version number
# It's run during the Cleanup stage so the build has been tested and verified
# before releasing
#
# This job requires the following variables be set
#   CI_BOT_API_TOKEN   User token that will commit the updated changelog file
#   RELEASE_BRANCH     Name of the branch releases should be made from
#                      Defaults to main
#   VERSION_FILE       File containing the packages version information
#
# You also need to provide an image containing any required parsing tools, jq
# and the curl binary
#
# The release-notes job supports various version file types by default and new
# one can be added by overriding the before_script step
# Ruby:
# extra variable required
#   GEMSPEC_FILE       Projects top level gemspec file
# image required
#   A ruby image such as ruby:2.7
#
# Rust:
#   PACKAGE_NAME       Name of the package in the manifest to get the
#                      version info for
# image required
#   An image containing both cargo and python3 such as the rust/java image
#
# C/C++:
#   DEFINE_NAME:       Name of the define that contains the version number
#
# image required
#   An image containing gcc
#
# YAML:
#   VERSION_VARIABLE:  Name of the variable that contains the version number
#
# image required
#   An image containing yq
#
# JSON:
#   VERSION_VARIABLE:  Name of the variable that contains the version number
#
# image required
#   An image containing jq or an image using apt as the package manager with
#   a jq package available

variables:
  RELEASE_BRANCH:
    value: $CI_DEFAULT_BRANCH
    description: The branch currently being used to create releases

.release-base:
  extends: .resource-request
  stage: Release
  rules:
    - if: '($CI_BOT_API_TOKEN == null ||
            $CI_PIPELINE_SOURCE == "schedule")'
      when: never
    - if: '$CI_PIPELINE_SOURCE == "web"'
      when: manual
      allow_failure: true
    - if: $CI_COMMIT_BRANCH == $RELEASE_BRANCH
      changes:
        - "CHANGELOG.md"
    - if: '$CI_MERGE_REQUEST_TITLE =~ /RELEASE/'
      when: manual
      allow_failure: true

release-notes:
  extends: .release-base
  before_script:
    - >
      sed_script="/^#define[ \t]+${DEFINE_NAME}/!d;
      s/#define[ \t]+[^ \t]+[ \t\"]+([^ \t\"]*).*/\1/"
    - |
      echo "Ensuring curl is available"
      if ! [ -x "$(command -v curl)" ]; then
        echo "curl binary not found attempting to install it"
        if [ -x "$(command -v apt)" ]; then
          apt update
          apt install --no-install-recommends -y curl ca-certificates
        elif [ -x "$(command -v apk)" ]; then
          apk --no-cache add curl
        fi
      fi
      echo "Ensuring jq is available"
      if ! [ -x "$(command -v jq)" ]; then
        echo "jq binary not found attempting to install it"
        if [ -x "$(command -v apt)" ]; then
          apt update
          apt install --no-install-recommends -y jq
        elif [ -x "$(command -v apk)" ]; then
          apk --no-cache add jq
        fi
      fi
      echo "Discovering version file type"
      filename="${VERSION_FILE##*/}"
      ext=${filename##*.}
      case $ext in
        rb )
          echo "Ruby: version file found"
          version=$(ruby -e \
            "print Gem::Specification.load('${GEMSPEC_FILE}').version")
          [ -n "${version}" ] || \
            (echo "VERSION could not be parsed in ${GEMSPEC_FILE}!" && exit 1)
          ;;
        toml )
          echo "Rust: suspected cargo toml file found"
          version=$(cargo metadata --format-version=1 --no-deps --offline \
            | jq -r ".packages[] | select(.name == \"${PACKAGE_NAME}\") \
              .version")
          ;;
        h )
          echo "C/C++: suspected c/c++ header file found"
          version=$(gcc -E -dM ${VERSION_FILE} | sed -r "{ ${sed_script} }" )
          ;;
        yml )
          echo "YAML: suspected yaml file found"
          version=$(yq eval "${VERSION_VARIABLE}" ${VERSION_FILE})
          ;;
        json )
          echo "JSON: suspected json file found"
          version=$(jq -r "${VERSION_VARIABLE}" ${VERSION_FILE})
          ;;
        * )
          echo "I don't know how to parse a $ext file, please provide your" \
            "own parser that set a variable call version in a before_script" \
            "section"
          ;;
      esac
      [ -n "${version}" ] \
        || (echo "VERSION could not be parsed in ${VERSION_FILE}!" && exit 1)
      echo "Detected version: ${version}"
  script:
    - |
      echo "Generating release notes for version: ${version}"
      changelog_url="${CI_API_V4_URL}/projects/"
      changelog_url+="${CI_PROJECT_ID}/repository/changelog"
      raw_release_notes=$(curl \
        --header "PRIVATE-TOKEN: ${CI_BOT_API_TOKEN}" \
        "${changelog_url}?version=${version}&to=${CI_COMMIT_SHA}" | jq ".notes")
      release_notes="${raw_release_notes%\"}"  # Remove trailing quote
      release_notes="${release_notes#\"}"  # Remove leading quote
      echo $release_notes
      echo "RELEASE_VERSION=${version}" > release_variables.env
      echo "RELEASE_TAG=v${version}" >> release_variables.env
      echo "RELEASE_NOTES=${release_notes}" >> release_variables.env
      cat release_variables.env
  artifacts:
    reports:
      dotenv: release_variables.env
    paths:
      - release_variables.env
    expire_in: 1 day

release-creation:
  extends: .release-base
  tags:
    - x86_64
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  needs: ["release-notes"]
  variables:
    RELEASENOTE_FILE: "release-note.md"
  script:
    - |
      # Transform "\n" to actual new lines
      echo "${RELEASE_NOTES}" | sed 's/\\n/\n/g' > "${RELEASENOTE_FILE}"
      echo "Creating the release for the ${RELEASE_TAG} tag with the release" \
        "notes from ${RELEASENOTE_FILE}:"
      cat "${RELEASENOTE_FILE}"
  release:
    name: "${RELEASE_TAG}"
    description: "${RELEASENOTE_FILE}"
    tag_name: "${RELEASE_TAG}"
  artifacts:
    paths:
      - "${RELEASENOTE_FILE}"
    expire_in: 30 days
